<?php

namespace Drupal\snapengage\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Displays the extlink settings form.
 */
class SnapengageSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'snapengage_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['account'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['account']['snapengage_widget_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Widget ID'),
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_widget_id'),
      '#description' => $this->t('Your widget ID. It will look somewhat like this: "abcdefgh-1234-ijkl-5678-mnopqrstuvwx". You can find it at <a href="@url">@url</a>.', ['@url' => 'https://secure.snapengage.com/widget']),
    ];

    $form['account']['snapengage_script'] = [
      '#type' => 'radios',
      '#title' => $this->t('Script'),
      '#options' => [
        'code.snapengage.com' => 'code.snapengage.com',
        'snapengage-eu' => 'snapengage-eu',
      ],
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_script') ?? 'code.snapengage.com',
    ];

    $form['account']['snapengage_widget_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Widget language'),
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_widget_language'),
      '#options' => [
        'default' => $this->t('Default language of site. With URL detection.'),
        'user' => $this->t("User's default language"),
      ],
      '#description' => $this->t('Select how to choose language.'),
    ];

    // Render the role overview.
    $form['role_vis_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Role specific visibility settings'),
      '#open' => TRUE,
    ];

    $roles = user_role_names();
    $form['role_vis_settings']['snapengage_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Role specific visibility'),
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_roles'),
      '#options' => $roles,
      '#description' => $this->t('Show widget only for the selected role(s). If you select none of the roles, then all roles will see the widget. If a user has any of the roles checked, the widget will be visible to the user.'),
    ];

    // Page specific visibility configurations.
    $form['page_vis_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Page specific visibility settings'),
      '#open' => TRUE,
    ];

    $form['page_vis_settings']['snapengage_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add widget to specific pages'),
      '#options' => [
        $this->t('Add to every page except the listed pages.'),
        $this->t('Add to the listed pages only.'),
      ],
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_visibility') ?? 1,
    ];

    $form['page_vis_settings']['snapengage_pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $this->configFactory->get('snapengage.settings')->get('snapengage_pages'),
      '#wysiwyg' => FALSE,
      '#description' => $this->t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. Remember to start every URL with /. Also if you have langauages enabed you nedd to set wildcard infront of the URLs, fex.: '*/admin/....'.",
                     [
                       '%blog' => 'blog',
                       '%blog-wildcard' => 'blog/*',
                       '%front' => '<front>',
                     ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('snapengage.settings')
      ->set('snapengage_widget_id', $values['snapengage_widget_id'])
      ->set('snapengage_script', $values['snapengage_script'])
      ->set('snapengage_widget_language', $values['snapengage_widget_language'])
      ->set('snapengage_roles', $values['snapengage_roles'])
      ->set('snapengage_visibility', $values['snapengage_visibility'])
      ->set('snapengage_pages', $values['snapengage_pages'])
      ->save();
    parent::SubmitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['snapengage.settings'];
  }

}
