CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The SnapEngage module allows the user to add the SnapEngage widget to the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/snapengage

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/snapengage


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * An account at snapengage.com


INSTALLATION
------------

 * Install the SnapEngage module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Create an account at snapengage.com.
    2. Navigate to Administration > Configuration > System > SnapEngage where
       you can enter your widget ID.
    3. Enter your widget ID. It will look somewhat like this:
       "abcdefgh-1234-ijkl-5678-mnopqrstuvwx". You can find it at
       https://secure.snapengage.com/widget.
    4. Set Visibility. Save configuration.


MAINTAINERS
-----------

 * Arne Jørgensen (arnested) - https://www.drupal.org/u/arnested
 * AndreaD - https://www.drupal.org/u/andread
 * Kasper Garnæs (kasperg) - https://www.drupal.org/u/kasperg

Supporting organization:

 * Reload - https://www.drupal.org/reload

The maintainers are not affiliated with SnapEngage.
